function getRandomInt(max){
    return Math.floor(Math.random() * max)
}

let f = getRandomInt(213)

console.log(`Temperatura em Farenheit: ${f}`)

let c = (f-32)/9 * 5

console.log(`Temperatura em Celsius: ${c.toFixed(4)}`)