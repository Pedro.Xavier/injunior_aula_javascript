function getRandomInt(max){
    return Math.floor(Math.random() * max)
}

console.log('=================================')
console.log('      ANALISADOR DE NUMEROS      ')
console.log('=================================')

//Para numeros divisíveis apenas por 3: fizz
//Para numeros divisíveis apenas por 5: buzz
//Para numeros divisíveis por 3 e por 5: fizzbuzz

for(i = 0; i < 5;i++){
    let n = getRandomInt(10001)
    if(n % 3 == 0 && n % 5 != 0){
        console.log('fizz')
    }
    else if(n % 5 == 0 && n % 3 != 0){
        console.log('buzz')
    }
    else if(n % 3 == 0 && n % 5 == 0){
        console.log('fizzbuzz')
    }
    else{
        console.log(n)
    }    
}