function getRandomInt(max){
    return Math.floor(Math.random() * max)
}

let N1 = getRandomInt(11)
let N2 = getRandomInt(11)
let N3 = getRandomInt(11)

let Media = (N1+N2+N3)/3
console.log(`Media: ${Media}`)

if(Media >= 6){
    console.log('Aprovado')
}
else{
    console.log('Reprovado')
}